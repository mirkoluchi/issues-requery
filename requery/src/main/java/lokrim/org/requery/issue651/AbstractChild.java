package lokrim.org.requery.issue651;

import io.requery.Entity;
import io.requery.Key;

/**
 * Created by mluchi on 04/08/2017.
 */

@Entity
public abstract class AbstractChild {

    @Key
    long id;

}
