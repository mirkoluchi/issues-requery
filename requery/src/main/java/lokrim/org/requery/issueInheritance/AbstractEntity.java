package lokrim.org.requery.issueInheritance;

import io.requery.Column;
import io.requery.Entity;
import io.requery.Key;
import io.requery.Superclass;

/**
 * Created by mluchi on 15/09/2017.
 */

@Entity
@Superclass
public class AbstractEntity {

    @Key
    protected long id;

    @Column
    protected String fieldOnEntity;

}
