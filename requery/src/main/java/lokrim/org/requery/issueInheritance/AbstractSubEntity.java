package lokrim.org.requery.issueInheritance;

import io.requery.*;

/**
 * Created by mluchi on 15/09/2017.
 */

@io.requery.Entity
public class AbstractSubEntity extends AbstractEntity{

    @Column
    protected String fieldOnSubEntity;

}
