package lokrim.org.requery;

import android.content.Context;
import android.support.test.InstrumentationRegistry;
import android.support.test.runner.AndroidJUnit4;

import org.junit.After;
import org.junit.Before;
import org.junit.runner.RunWith;

import javax.annotation.Nonnull;

import io.requery.android.sqlite.DatabaseSource;
import io.requery.cache.EmptyEntityCache;
import io.requery.meta.EntityModel;
import io.requery.sql.Configuration;
import io.requery.sql.ConfigurationBuilder;
import io.requery.sql.EntityDataStore;
import io.requery.sql.platform.SQLite;
import lokrim.org.requery.issue651.*;

import static junit.framework.Assert.assertTrue;

/**
 * Created by mluchi on 04/08/2017.
 */
@RunWith(AndroidJUnit4.class)
public abstract class AbstractRequeryTest {

    private static final String DATABASE_NAME = "test";
    private EntityDataStore entityDataStore;

    @Nonnull
    protected abstract EntityModel getModel();

    @Before
    public void setUp() throws Exception {
        EntityModel entityModel = getModel();
        DatabaseSource dataSource = new DatabaseSource(getContext(), entityModel, DATABASE_NAME, 1);
        dataSource.setLoggingEnabled(true);
        SQLite platform = new SQLite();
        Configuration configuration = new ConfigurationBuilder(dataSource, entityModel)
                .setPlatform(platform)
                .setEntityCache(new EmptyEntityCache()) // NO CACHE
                .build();
        this.entityDataStore = new EntityDataStore(configuration);
    }

    @After
    public void tearDown() throws Exception {
        deleteDatabaseIfExists(DATABASE_NAME);
    }

    protected Context getContext() {
        return InstrumentationRegistry.getTargetContext();
    }

    protected EntityDataStore getEntityDataStore() {
        return entityDataStore;
    }

    protected void deleteDatabaseIfExists(@Nonnull String name) {
        if (getContext().getDatabasePath(name).exists()) {
            boolean deleted = getContext().deleteDatabase(name);
            assertTrue(deleted);
        }
    }

}
