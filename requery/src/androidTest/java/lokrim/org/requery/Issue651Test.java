package lokrim.org.requery;

import org.junit.Test;

import javax.annotation.Nonnull;

import io.requery.meta.EntityModel;
import lokrim.org.requery.issue651.Child;
import lokrim.org.requery.issue651.Parent;

public class Issue651Test extends AbstractRequeryTest {

    @Nonnull
    @Override
    protected EntityModel getModel() {
        return Models.DEFAULT;
    }

    @Test(expected = Exception.class) // I expect a Foreign Key Constraint Violation
    public void testInsertDoesNotCascadeIfCascadeActionSaveNotSpecified() {
        Child child = new Child();
        child.setId(1);
        Parent parent = new Parent();
        parent.setChild(child);

        // I expect this method to throw an exception because child does not exist and CascadeAction.SAVE is not specified
        getEntityDataStore().insert(parent);
    }

}
